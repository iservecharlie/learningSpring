package org.web.learning.spring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("*")
public class LandingController{

    @RequestMapping(value = {"*", "index"}, method = {RequestMethod.GET, RequestMethod.POST})
    public String landingPage (){
        return "index";
    }
}
